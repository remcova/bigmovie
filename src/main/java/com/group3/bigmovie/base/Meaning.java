package com.group3.bigmovie.base;


public final class Meaning
{
    public static final int AGE = 0;
    public static final int AMOUNT_DIRECTED = 1;
    public static final int AMOUNT_PLAYED = 2;
    public static final int WHEN_RELEASED = 3;
    public static final int WHERE_FILMED = 4;
    public static final int WHAT_BUDGET = 5;
    public static final int WHO_PLAYED = 6;
    public static final int WHO_WROTE = 7;
    public static final int WHAT_ABOUT = 8;
    public static final int RECOMMEND = 9;
    public static final int ACTOR_TO_ACTRESS = 10;
    public static final int ABOVE_AGE = 11;
    public static final int WHAT_RATING = 12;
}